Set up :


Add the follow to the JVM arguments of Sample.					
-javaagent:/home/matmsh/.m2/repository/org/aspectj/aspectjweaver/1.7.0/aspectjweaver-1.7.0.jar
-Djava.library.path=/home/matmsh/.m2/repository/org/fusesource/sigar/1.6.4/sigar-1.6.4.jar
-Dorg.aspectj.tracing.factory=default


Need to start TypeSafe console before the Akka application.

1)     bin/typesafe-console atmos

2)     bin/typesafe-console ui


go to http://localhost:9900/.