package com.typesafe.atmos.sample



import akka.actor._
import scala.concurrent.duration._
import java.util.concurrent.TimeUnit
import com.typesafe.config.ConfigFactory

object Sample extends App {
  val system = ActorSystem("SimpleApp")
  
   //val system = ActorSystem("SimpleApp", ConfigFactory.parseString("akka.scheduler.tick-duration = 1ms"))
   
  val pingActor = system.actorOf(Props[PingActor].withDispatcher("my-pinned-dispatcher"), "pingActor")
  implicit val exec = system.dispatcher
  //system.scheduler.schedule(0 seconds, 1 seconds, pingActor, Ping)
  
  //The default timer in the Actor scheduler has a minimum pause of 100ms. 
  system.scheduler.schedule(0 seconds, Duration(1, TimeUnit.MILLISECONDS), pingActor, Ping)
  
  
  //(new Runner(pingActor)).start()

  
}

class Runner(pingActor:ActorRef) extends Thread {
  
 
  override def run():Unit={
     
     for(i <- 0 to 1000000000){
         pingActor ! Ping
         Thread.sleep(0, 100000)
     }
  }
  
}

case object Ping

class PingActor extends Actor {
  var count =1
  def receive = {
    case Ping => {
       //println("Pinged at: " + System.currentTimeMillis)
       count = count +1
       if (count % 1000==0) println("count = " + count )
     
    }
  }
}



